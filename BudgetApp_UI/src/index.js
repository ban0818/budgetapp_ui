import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import {createTheme,ThemeProvider} from '@material-ui/core'
import grey from '@material-ui/core/colors/grey'

const theme = createTheme({
  palette:{
    primary:{
      main: grey[800]
    },
    secondary:{
      main: grey[600]
    },
    text:{
      main: grey[50]
    },
    text2:{
      main: grey[300]
    },
    accent:{
      main: grey[500]
    },
    accent2:{
      main: grey[900]
    }
  }
})

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
