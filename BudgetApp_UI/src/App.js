import './App.css';
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import InputButtons from './InputButtons';
import InfoDisplay from './InfoDisplay';
import styled from 'styled-components';
import Header from './Header.js'
import PurchaseForm from './PurchaseForm';
import {Grid,Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

//Styles used for the main grid
const useStyles = makeStyles((theme) => ({
  button:{
      margin: '3px'
  },
  background:{
    padding: '10px',
    marginTop: '5px',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.text.main
  }
}))

//Style used for the table displaying spending history
const Styles = styled.div`
    padding: 1rem;
    table {
        border-spacing: 0;
        border: 1px solid black;
        tr {
            :last-child {
                td {
                border-bottom: 0;
                }
            }
            }
        
            th,
            td {
            margin: 0;
            padding: 0.5rem;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
        
            :last-child {
                border-right: 0;
            }
        }
    }
`

function App() {

  //Hooks
  const[displayOut,setDisplayOut] = useState([]);
  const[chooseMonth,setChooseMonth] = useState(false);
  const[enterPurchase,setEnterPurchase] = useState(false);
  const[amount,setAmount] = useState(0);
  const[item,setItem] = useState('');
  const[notes,setNotes] = useState('');
  const[total,setTotal] = useState([]);
  const[entryCount,setEntryCount] = useState(0);
  const[resetTotal,setresetTotal] = useState(false);
  
  //Get the amount of entries so we can get the most recent total
  useEffect(async () => {
    await axios.get('http://localhost:8081/budget/count').then(res => {const response = res.data; setEntryCount(response);})
  },[]);


  //Button Names
  const buttons = [
    {id: 1, title: "Record Purchase" },
    {id: 2, title: "Monthly History"},
    {id: 3, title: "Complete History"},
    {id: 4, title: "New Month"},
  ]

  //Month names for monthly spending
  const months = [
    {id: 1, title: "JAN" },
    {id: 2, title: "FEB"},
    {id: 3, title: "MAR"},
    {id: 4, title: "APR"},
    {id: 5, title: "MAY"},
    {id: 6, title: "JUN"},
    {id: 7, title: "JUL"},
    {id: 8, title: "AUG"},
    {id: 9, title: "SEPT"},
    {id: 10, title: "OCT"},
    {id: 11, title: "NOV"},
    {id: 12, title: "DEC"},
  ]

  //Columns to display in the table showing spending
  const columns = React.useMemo(
    () => [
      {
        Header: 'Spending History',
        columns:[
          {
            Header: 'Amount Spent',
            accessor: 'amount',
          },
          {
            Header: "total Remaining",
            accessor: 'total'
          },
          {
            Header: 'Date',
            accessor: 'date',
          },
          {
            Header: 'Item',
            accessor: 'item',
          },
          {
            Header: 'Month',
            accessor: 'month',
          },
          {
            Header: 'Notes',
            accessor: 'notes',
          },
        ],
      },
    ],
    []
  )


  //Function to call when button clicked
  const chooseWhatToDo = (selection) => {
    ResetState();
    switch(selection){
      case "Record Purchase":
        setEnterPurchase(true);
        //If this is the only entry in the database
        if(entryCount === 0){
          setresetTotal(true);
        }else{ //Get the most recent total
          axios.get('http://localhost:8081/budget').then(res => {const response = res.data[entryCount-1]; setTotal(response);})
        }
        break;
      case "Monthly History":
        setChooseMonth(true);
        break;
      case "Complete History":
        //Get entire spending history from database
        axios.get('http://localhost:8081/budget').then(res => {const response = res.data; setDisplayOut(response);})
        break;
      default:
        setresetTotal(true);
        break;
    }
  }

  //Get Spending for specific month
  const onMonthSelect = (month) => {
    axios.get('http://localhost:8081/budget'+"/"+month).then(res => {const response = res.data; setDisplayOut(response);})
  }


  //Send form information to database
  const onSubmitClick = async () => {
    //Get the current date in mm/dd/yyyy format
    const newDate = new Date();
    const CurMonth = newDate.toLocaleString('default',{month:'short'}).toUpperCase();
    const dd = String(newDate.getDate()).padStart(2,'0');
    const mm = String(newDate.getMonth()+1).padStart(2,'0');
    const yyyy = newDate.getFullYear();
    const today = mm+"/"+dd+"/"+yyyy;

    //If we are at the beginning of the month then reset the total
    if(dd === "01"){
      setresetTotal(true);
    }

    //Use past total and amount spent to determine the new total to store
    let temptotal;
    if(resetTotal){
      temptotal = 2000 - amount;
      setresetTotal(false);
    }
    else{
      temptotal = total['total']-amount;
    }


    //Post to the databse with our information
    await axios({
      method: 'post',
      url: 'http://localhost:8081/budget',
      data: {
        "month": CurMonth,
        "date":  today,
        "amount": amount,
        "item": item,
        "notes": notes,
        "total": temptotal,
      }
    });

    //Reset the page
    ResetState();

  }

  //Update states as you type in input fields
  const onAmountChange = (event) => {
    setAmount(event.target.value);
  }
  const onItemChange = (event) => {
    setItem(event.target.value);
  }
  const onNotesChange = (event) => {
    setNotes(event.target.value);
  }

  //Reset the page
  const ResetState = () => {
    setChooseMonth(false);
    setEnterPurchase(false);
    setDisplayOut([]);
    setAmount();
    setItem();
    setNotes();
  }
  
  const classes = useStyles();

  return (
    <Grid container justify='center' direction='column' alignItems='center' spacing={10}>
      <Grid item xs={6}>
        <Header></Header>
      </Grid>
      <Grid item xs={7}>
        <Grid container component={Paper} className={classes.background} direction='column' alignItems='center' justify='center' spacing={2}>
          <Grid item>
            <InputButtons buttonNames={buttons} onSelection={chooseWhatToDo}/>
          </Grid>
          <Grid item>
            <h4>{chooseMonth ? <div>
              <InputButtons buttonNames={months} onSelection={onMonthSelect}/>
            </div>:null}</h4>
          </Grid>
          <Grid item>
            <h4>{enterPurchase ? <PurchaseForm onAmount={onAmountChange} onNotes={onNotesChange} onItem={onItemChange} amount={amount} notes={notes} item={item} onSubmitClick={onSubmitClick}></PurchaseForm>:null}</h4>
          </Grid>
          <Styles>
            <InfoDisplay columns={columns} data={displayOut} />
          </Styles>
        </Grid>
      </Grid>
    </Grid>
  );
  }

export default App;
