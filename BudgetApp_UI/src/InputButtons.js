import React from 'react'
import {Button} from '@material-ui/core'
import {makeStyles} from '@material-ui/core'

//Style used for buttons
const useStyles = makeStyles((theme) => ({
    //Used to keep all buttons in consitent style
    button:{
        margin: '3px',
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.text.main
    }
}))

export default function InputButtons({buttonNames,onSelection}) {
    const classes = useStyles();
    return buttonNames.map(buttonName => <Button variant='contained' className={classes.button} key={buttonName.id} onClick={()=> onSelection(buttonName.title)}>{buttonName.title}</Button>);
}