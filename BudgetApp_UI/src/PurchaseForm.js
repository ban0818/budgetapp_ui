import React from 'react'
import {Grid, Paper,Button} from '@material-ui/core';
import { makeStyles } from '@material-ui/core';


//Style used for each element
const useStyles = makeStyles((theme) =>({
    //used for background of the form backing color
    background:{
        backgroundColor: theme.palette.accent.main,
    },
    //used for the forms text color
    formBackground:{
        color: theme.palette.primary.main
    },
    //Used to keep all buttons designed the same
    button:{
        margin: '3px',
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.text.main
    }
}))


export default function PurchaseForm({onAmount,onNotes,onItem,amount,notes,item,onSubmitClick}){
    const classes = useStyles();
    return(
        <Grid container component={Paper} className={classes.background}justify='center' direction='column' alignItems='center' spacing={2}>
            <Grid item>
                <form className={classes.formBackground}>
                    <Grid container justify='center' direction='column' alignItems='center' spacing={3}>
                            <Grid item>
                                <label>
                                    Amount:
                                    <input type='text' value={amount} onChange={onAmount}/>
                                </label>
                            </Grid>
                            <Grid item>
                                <label>
                                    Item:
                                    <input type='text' value={item} onChange={onItem}/>
                                </label>
                            </Grid>
                            <Grid item>
                                <label>
                                    Notes:
                                    <input type='text' value={notes} onChange={onNotes}/>
                                </label>
                            </Grid>
                            <Grid item>
                                <Button variant='contained' className={classes.button} key={1} onClick={onSubmitClick} onSubmit={onSubmitClick}>Submit</Button>
                            </Grid>
                    </Grid>
                </form>
            </Grid>
        </Grid>
    )
}