import React from 'react';
import {useTable} from 'react-table';
import {Grid,Paper} from '@material-ui/core';
import{makeStyles} from '@material-ui/core';

//Style for coloring the table
const useStyles = makeStyles((theme) =>({
    //Color the background of the table
    tableBackground:{
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.text2.main
    },
    //Color the paper that the table sits on
    paperBackground:{
        backgroundColor: theme.palette.accent.main,
    }
}))

export default function InfoDisplay({data,columns}) {
    const classes = useStyles();
    //Used to construct table
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({
        columns,
        data,
    })
    //If there is no information in the database then don't display a table
    if(data.length === 0){
        return <></>
    }
    else{
        return (
            <Grid container component={Paper} className={classes.paperBackground} justify='center' direction='column' alignItems='center' spacing={2}>
                <Grid item>
                    <table {...getTableProps()} className={classes.tableBackground}>
                        <thead>
                            {headerGroups.map(headerGroup => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map(column => (
                                        <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        <tbody {...getTableBodyProps()}>
                            {rows.map((row,i) => {
                                prepareRow(row)
                                return(
                                    <tr {...row.getRowProps()}>
                                        {row.cells.map(cell => {
                                            return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                        })}
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </Grid>
            </Grid>
        )
    }
}