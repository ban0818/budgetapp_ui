import React from 'react'

import { Grid,Paper,Typography } from '@material-ui/core'
import{makeStyles} from '@material-ui/core'

//Style used for the backing of the header
const useStyles = makeStyles((theme) =>({
    background:{
        padding: '10px',
        marginTop: '5px',
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.text.main
    }
}))

const Header = () => {
    const classes = useStyles();
    return(
        <Grid contain justify="center">
            <Grid item>
                <Paper className={classes.background} elevation={5}>
                    <Typography variant='h3'>
                        What would you like to do?
                    </Typography>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default Header;